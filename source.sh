# This is the source.sh script. It is executed by BPM in a temporary directory when compiling a source package
# BPM Expects the source code to be extracted into the automatically created 'source' directory which can be accessed using $BPM_SOURCE
# BPM Expects the output files to be present in the automatically created 'output' directory which can be accessed using $BPM_OUTPUT

# The package function is executed in the source directory
# This function is used to move the compiled files into the output directory
package() {
  mkdir -p "$BPM_OUTPUT"/etc
  mkdir -p "$BPM_OUTPUT"/var
  mkdir -p "$BPM_OUTPUT"/usr
  mkdir -p "$BPM_OUTPUT"/dev
  mkdir -p "$BPM_OUTPUT"/proc
  mkdir -p "$BPM_OUTPUT"/sys
  mkdir -p "$BPM_OUTPUT"/run
  mkdir -p "$BPM_OUTPUT"/tmp
  mkdir -p "$BPM_OUTPUT"/boot
  mkdir -p "$BPM_OUTPUT"/opt
  mkdir -p "$BPM_OUTPUT"/srv
  mkdir -p "$BPM_OUTPUT"/mnt
  mkdir -p "$BPM_OUTPUT"/home

  mkdir -p "$BPM_OUTPUT"/etc/{opt,sysconfig}
  mkdir -p "$BPM_OUTPUT"/usr/lib/firmware
  mkdir -p "$BPM_OUTPUT"/media/{floppy,cdrom}
  mkdir -p "$BPM_OUTPUT"/usr/{,local/}{include,src}
  mkdir -p "$BPM_OUTPUT"/usr/{,local/}{bin,lib,lib64,sbin}
  mkdir -p "$BPM_OUTPUT"/usr/{,local/}share/{color,dict,doc,info,locale,man}
  mkdir -p "$BPM_OUTPUT"/usr/{,local/}share/{misc,terminfo,zoneinfo}
  mkdir -p "$BPM_OUTPUT"/usr/{,local/}share/man/man{1..8}
  mkdir -p "$BPM_OUTPUT"/var/{cache,local,log,mail,opt,spool}
  mkdir -p "$BPM_OUTPUT"/var/lib/{color,misc,locate}

  install -d -m 0750 "$BPM_OUTPUT"/root
  install -d -m 1777 /tmp "$BPM_OUTPUT"/var/tmp

  ln -s usr/bin "$BPM_OUTPUT"/bin
  ln -s usr/sbin "$BPM_OUTPUT"/sbin
  ln -s usr/lib "$BPM_OUTPUT"/lib
  ln -s usr/lib64 "$BPM_OUTPUT"/lib64
  ln -s /proc/self/mounts "$BPM_OUTPUT"/etc/mtab
  ln -s /run "$BPM_OUTPUT"/var/run
  ln -s /run/lock "$BPM_OUTPUT"/var/lock
  ln -s bash "$BPM_OUTPUT"/bin/sh

  touch "$BPM_OUTPUT"/var/log/{btmp,lastlog,faillog,wtmp}
  chgrp utmp "$BPM_OUTPUT"/var/log/lastlog
  chmod 664  "$BPM_OUTPUT"/var/log/lastlog
  chmod 600  "$BPM_OUTPUT"/var/log/btmp

  cp "$BPM_WORKDIR"/hosts "$BPM_OUTPUT"/etc/hosts
  cp "$BPM_WORKDIR"/group "$BPM_OUTPUT"/etc/group
  cp "$BPM_WORKDIR"/passwd "$BPM_OUTPUT"/etc/passwd
  cp "$BPM_WORKDIR"/gshadow "$BPM_OUTPUT"/etc/gshadow
  cp "$BPM_WORKDIR"/shadow "$BPM_OUTPUT"/etc/shadow
  cp "$BPM_WORKDIR"/nsswitch.conf "$BPM_OUTPUT"/etc/nsswitch.conf
  cp "$BPM_WORKDIR"/os-release "$BPM_OUTPUT"/etc/os-release
  cp "$BPM_WORKDIR"/lsb-release "$BPM_OUTPUT"/etc/lsb-release
  cp "$BPM_WORKDIR"/shells "$BPM_OUTPUT"/etc/shells

  echo "tidelinux" > "$BPM_OUTPUT"/etc/hostname
}
